﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary1;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            CurvedTrapezoid curvedTrapezoid = new CurvedTrapezoid(10, 1, 4);
            Console.WriteLine("Создана базовая криволинейная трапеция, образуемая гиперболой и осью ОХ, с границами от 1 до 10 и коэффициентом 4");
            int choice;
            do
            {
                Console.WriteLine("-------------------------------------------------------------------");
                Console.WriteLine("              Меню              ");
                Console.WriteLine("Выберите пункт меню:");
                Console.WriteLine("1:Вывести длины сторон, площадь и периметр криволинейной трапеции;");
                Console.WriteLine("2:Проверить принадлежит ли точка криволинейной трапеции;");
                Console.WriteLine("3:Инициализация новой гипербалической трапеции;");
                Console.WriteLine("4:Выход;");
                Console.WriteLine("-------------------------------------------------------------------");
                while (!Int32.TryParse(Console.ReadLine(), out choice))
                {
                    Console.WriteLine("Неверный ввод!");
                }
                Console.WriteLine();
                switch (choice)
                {
                    case 1:
                        Console.WriteLine(curvedTrapezoid.GetResults());
                        break;
                    case 2:
                        double x;
                        double y;
                        Console.WriteLine("Введите Х: ");
                        
                        while (!Double.TryParse(Console.ReadLine(), out x))
                        {
                            Console.WriteLine("Неверный ввод!");
                            Console.Write("Введите X: ");
                        }
                        Console.WriteLine("Введите Y: ");
                        while (!Double.TryParse(Console.ReadLine(), out y))
                        {
                            Console.WriteLine("Неверный ввод!");
                            Console.Write("Введите Y");
                        }
                        Console.WriteLine(curvedTrapezoid.IsPointLocation(x, y));
                        break;
                    case 3:
                        double upperLimit;
                        double lowerLimit;
                        double koef;

                        Console.WriteLine("Попытка инициализации криволинейной трапеции... ");
                        if (IsCorrectLimit(out upperLimit, out lowerLimit, out koef))
                        {
                            curvedTrapezoid = new CurvedTrapezoid(upperLimit, lowerLimit, koef);
                            Console.WriteLine("Инициализация успешна!");
                        }
                        else
                        {
                            Console.WriteLine("Создана требуемая трапеция");
                        }
                        break;
                    case 4:
                        break;
                    default:
                        Console.WriteLine("Неверное значение!");
                        break;

                }
            } while (choice != 4);
            Console.ReadKey();
        }

        public static bool IsCorrectLimit(out double UpperLimit, out double LowerLimit, out double Koef)
        {
            bool isCorrect = true;

            Console.Write("Введите координату X верхнего предела A: ");
            while (!Double.TryParse(Console.ReadLine(), out UpperLimit))
            {
                Console.WriteLine("Неверный ввод!");
                Console.Write("Введите координату X верхнего предела A: ");
            }

            Console.Write("Введите координату X нижнего предела B: ");
            while (!Double.TryParse(Console.ReadLine(), out LowerLimit))
            {
                Console.WriteLine("Неверный ввод!");
                Console.Write("Введите координату X верхнего предела B: ");
            }

            Console.Write("Введите коэффициент k: ");
            while (!Double.TryParse(Console.ReadLine(), out Koef))
            {
                Console.WriteLine("Неверный ввод!");
                Console.Write("Введите коэффицикет K: ");
            }


            if (LowerLimit >= UpperLimit)
            {
                Console.WriteLine("Error: A должно быть больше B !");
                isCorrect = false;
            }
            else
                if (UpperLimit == 0 || LowerLimit == 0 || (LowerLimit <= 0 && UpperLimit >= 0))
                {
                    Console.WriteLine("Error: Фигура не существует.");
                    isCorrect = false;
                }

            return isCorrect;
        }
    }
}
