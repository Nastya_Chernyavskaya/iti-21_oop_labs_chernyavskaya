﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Report_Lib;

namespace Console_Lab5
{
    class Program
    {
        static void Main(string[] args)
        {
            StringBuilder[] reps = {
                new StringBuilder("В районе рынка держинского автомобиль BY AOS3133 столкнулся с автомобилем"),
                //new StringBuilder("DTP: Car number: RUS MS1234"),
                new StringBuilder("Автомобиль регистрации CZ OS2874 сошёл с трассы из-за неуправляемости"),
                new StringBuilder("Автомобиль  BY AZ2735 в результате скольжения столкнулся с автомобилем")
                //new StringBuilder("DTP: Car number: RUS 1111"),
                //new StringBuilder("DTP: Car number: RUS 1435")
            };

            Report[] reports = new Report[reps.Length];
            for (int i = 0; i < reps.Length; i++)
            {
                reports[i] = new Report(reps[i]);
            } 
            Array.Sort(reports);
            foreach (var item in reports)
            {
                Console.WriteLine($"Country: {item.Country}, Number: {item.Number}");
            }
            Console.ReadKey();
        }
    }
}
