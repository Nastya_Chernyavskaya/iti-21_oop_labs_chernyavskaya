﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Lab8;

namespace WpfLab8
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Controler controler;
        public MainWindow()
        {
            controler = new Controler();
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Info.Items.Clear();
            try
            {
                controler.LoadInfo(fileName.Text);
                foreach (var item in controler.transport)
                {
                    var tmp = new TextBlock();
                    tmp.Text = item.GetInfo();
                    tmp.TextWrapping = TextWrapping.Wrap;
                    if (item is Plane)
                    {
                        tmp.Background = new SolidColorBrush(Colors.Red);
                    }
                    else if (item is Train)
                    {
                        tmp.Background = new SolidColorBrush(Colors.Blue);
                    }
                    Info.Items.Add(tmp);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Ведины некорректные данные!");
            }

        }
    }
}
