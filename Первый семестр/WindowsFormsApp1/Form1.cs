﻿using MathArrayLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        ArrProcesser Parse(string str)
        {
            string[] nums = str.Split(' ');
            double[] arr = new double[nums.Length];
            for (int i = 0; i < nums.Length; i++)
            {
                arr[i] = double.Parse(nums[i]);
            }
            return new ArrProcesser(arr);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Output.Text = (Parse(InputArray1.Text) + Parse(InputArray2.Text)).ToString();
            }
            catch
            {
                MessageBox.Show("Invalid input!");
            }
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                Output.Text = (Parse(InputArray1.Text) * double.Parse(InputArray2.Text)).ToString();
            }
            catch
            {
                MessageBox.Show("Invalid input!");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                Output.Text = (Parse(InputArray1.Text) / double.Parse(InputArray2.Text)).ToString();
            }
            catch (DivByZeroException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch
            {
                MessageBox.Show("Invalid input!");
            }
        }
    }
}
