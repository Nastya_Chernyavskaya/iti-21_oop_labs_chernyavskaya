﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatrixLib
{
    public class Matrix
    {
        public uint Row
        {
            get
            {
                return (uint)mtrx.GetLength(0);
            }
        }

        public uint Col
        {
            get
            {
                return (uint)mtrx.GetLength(1);
            }
        }

        private float[,] mtrx;

        public float this[int i, int j]
        {
            get
            {
                return mtrx[i, j];
            }
            set
            {
                mtrx[i, j] = value;
            }
        }

        public Matrix()
        {
            mtrx = new float[0, 0];
        }

        public Matrix(float[,] m)
        {
            mtrx = m;
        }

        public Matrix(uint row, uint col)
        {
            mtrx = new float[row, col];
        }

        public int SearchMinElement(int number)
        {

            double minElement = mtrx[0, 0];
            int res = 0;

            for (int i = 0; i < Row; i++)
            {
                for (int j = 0; j < Col; j++)
                {
                    if ((i +1) % number == 0)
                    {
                        if (minElement > mtrx[i, j])
                        {
                            minElement = mtrx[i, j];
                            res = i;
                        }
                    }
                }
            }
            return res;//возвращаем строку  минимального элемента в матрице в строке кратной n
        }

        public double SumSquaresElements(int number)
        {
            double summa = 0;
            int min_i = SearchMinElement(number) + 1;
            for (int i = min_i; i < Row; i++)
            {
                for (int j = 0; j < Col; j++)
                {
                    if (mtrx[i, j] < 0)
                    {
                        summa += mtrx[i, j] * mtrx[i, j];
                    }

                }
            }
            return summa;
        }

        public float SumSquaresElements()//метод без параметров
        {
            float summa = 0;
            for (int i = 0; i < Row; i++)
            {
                for (int j = 0; j < Col; j++)
                {
                    if (mtrx[i, j] < 0)
                    {
                        summa += mtrx[i, j] * mtrx[i, j];
                    }
                }
            }
            return summa;
        }

        public static Matrix operator -(Matrix mtrxA, Matrix mtrxB)
        {
            Matrix result = new Matrix(mtrxA.Row, mtrxB.Col);
            if (mtrxA.Col == mtrxB.Col & mtrxA.Row == mtrxB.Row)
            {
                for (int i = 0; i < mtrxA.Row; i++)
                {
                    for (int j = 0; j < mtrxA.Col; j++)
                    {
                        result[i, j] = mtrxA.mtrx[i, j] - mtrxB.mtrx[i, j];
                    }
                }
            }
            else
            {
                throw new Exception("Неверный размер матриц!");
            }
            return result;
        }

        public static bool operator >=(Matrix mtrxA, Matrix mtrxB)
        {
            GetSum(mtrxA, mtrxB, out float summaA, out float summaB);
            return summaA >= summaB ? true : false;
        }

        static void GetSum(Matrix mtrxA, Matrix mtrxB, out float summaA, out float summaB)
        {
            summaA = 0;
            summaB = 0;

            foreach (var item in mtrxA.mtrx)
            {
                if (item > 0)
                {
                    summaA += item * item;
                }
            }

            foreach (var item in mtrxB.mtrx)
            {
                if (item > 0)
                {
                    summaB += item * item;
                }
            }
        }

        public static bool operator <=(Matrix mtrxA, Matrix mtrxB)
        {
            GetSum(mtrxA, mtrxB, out float summaA, out float summaB);
            return summaA <= summaB ? true : false;
        }

        public static float Sum_El_C(int res, Matrix mtrx)
        {
            float summa = 0;
            int min_i = res + 1;
            for (int i = min_i; i < mtrx.Row; i++)
            {
                for (int j = 0; j < mtrx.Col; j++)
                {
                    if (mtrx[i, j] > 0)
                    {
                        summa += mtrx[i, j] * mtrx[i, j];
                    }
                }
            }
            return summa;
        }

        public static Matrix Res(Matrix mtrx, float summa)
        {
            for (int i = 0; i < mtrx.Row; i++)
            {
                for (int j = 0; j < mtrx.Col; j++)
                {
                    if (mtrx[i, j] < 0)
                    {
                        mtrx[i, j] = summa;
                    }
                }
            }
            return mtrx;
        }
    }
}
