﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab8
{
    public abstract class Transport
    {
        public abstract int GetEmptySeats();
        public string from;
        public string to;
        public TransportType type;
        public virtual string GetInfo()
        {
            return "Тип транспорта: " + type + " пункт отправления: " + from + " пункт назначения: " + to + " количество свободных мест: " + GetEmptySeats();
        } 
    }

    public enum TransportType
    {
        GroundTransportation, 
        AirTransportation,
        RailwayTransportation
    }
}
