﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab8
{
    public class Train : GroundTransport
    {
        public int[] emptySeatsOnRailway;

        public Train()
        {
            type = TransportType.RailwayTransportation;
            ticketPrice = new float[4];
        }

        public override string GetInfo()
        {
            return "Поезд. Цена на люкс: " + ticketPrice[0] + " цена на купейный: " + ticketPrice[1] + " цена на плацкартный: " + ticketPrice[2] + " цена на общий: " + ticketPrice[3] + base.GetInfo();
        }

        public float this[string kind]
        {
            get
            {
                switch (kind)
                {
                    case "Люкс":
                        return ticketPrice[0];
                    case "Купейный":
                        return ticketPrice[1];
                    case "Плацкартный":
                        return ticketPrice[2];
                    case "Общий":
                        return ticketPrice[3];
                    default:
                        throw new Exception();
                }
            }
            set
            {
                switch (kind)
                {
                    case "Люкс":
                        ticketPrice[0] = value;
                        break;
                    case "Купейный":
                        ticketPrice[1] = value;
                        break;
                    case "Плацкартный":
                        ticketPrice[2] = value;
                        break;
                    case "Общий":
                        ticketPrice[3] = value;
                        break;
                    default:
                        throw new Exception("Тип мест не обнаружен!");
                }
            }
        }

        public override int GetEmptySeats()
        {
            int summ = 0;
            foreach (int item in emptySeatsOnRailway)
            {
                summ += item;
            }
            return summ;
        }
    }
}
