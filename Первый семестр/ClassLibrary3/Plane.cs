﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab8
{
    public class Plane : Transport
    {
        public override int GetEmptySeats()
        {
            return emptySeats;
        }

        public override string GetInfo()
        {
            return "Самолёт. Цена на эконом класс: " + ticketPrice[0] + " цена на бизнес класс: " + ticketPrice[1] + " цена на первый класс: " + ticketPrice[2] + base.GetInfo();
        }

        public Plane()
        {
            type = TransportType.AirTransportation;
            ticketPrice = new float[3];
        }

        public int emptySeats;
        public int numberOfFlight;
        public float[] ticketPrice;

        public float this[string kind]
        {
            get
            {
                switch (kind)
                {
                    case "Эконом":
                        return ticketPrice[0];
                    case "Бизнес":
                        return ticketPrice[1];
                    case "Первый":
                        return ticketPrice[2];
                    default:
                        throw new Exception();
                }
            }
            set
            {
                switch (kind)
                {
                    case "Эконом":
                        ticketPrice[0] = value;
                        break;
                    case "Бизнес":
                        ticketPrice[1] = value;
                        break;
                    case "Первый":
                        ticketPrice[2] = value;
                        break;
                    default:
                        throw new Exception("Тип мест не обнаружен!");
                }
            }
        }

    }
}
