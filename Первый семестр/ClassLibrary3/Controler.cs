﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab8
{
   public class Controler
    {
        public  List<Transport> transport;

        public Controler()
        {
            transport = new List<Transport>();
        }

        public void LoadInfo(string fileName)
        {
            using (StreamReader fs = new StreamReader(fileName))
            {
                while (!fs.EndOfStream)
                {
                    var tmp = fs.ReadLine().Split(' ');
                    switch (tmp[0])
                    {
                        case "Самолёт":
                            {
                                var transp = new Plane();
                                transp.numberOfFlight = int.Parse(tmp[1]);
                                transp.from = tmp[2];
                                transp.to = tmp[3];
                                transp.ticketPrice[0] = float.Parse(tmp[4]);
                                transp.ticketPrice[1] = float.Parse(tmp[5]);
                                transp.ticketPrice[2] = float.Parse(tmp[6]);
                                transp.emptySeats = 500;
                                transport.Add(transp);
                            }
                            break;
                        case "Автобус":
                            {
                                var transp = new Bus();
                                transp.number = int.Parse(tmp[1]);
                                transp.from = tmp[2];
                                transp.to = tmp[3];
                                transp.emptySeats = 30;
                                transp.ticketPrice[0] = float.Parse(tmp[4]);
                                transp.ticketPrice[1] = float.Parse(tmp[5]);
                                transport.Add(transp);
                            }
                            break;
                        case "Поезд":
                            {
                                var transp = new Train();
                                transp.number = int.Parse(tmp[1]);
                                transp.from = tmp[2];
                                transp.to = tmp[3];
                                transp.ticketPrice[0] = float.Parse(tmp[4]);
                                transp.ticketPrice[1] = float.Parse(tmp[5]);
                                transp.ticketPrice[2] = float.Parse(tmp[6]);
                                transp.ticketPrice[3] = float.Parse(tmp[7]);
                                transp.emptySeatsOnRailway = new int[6];
                                for (int i = 0; i < 6; i++)
                                {
                                    transp.emptySeatsOnRailway[i] = 40;
                                }
                                transport.Add(transp);
                            }
                            break;
                        default:
                            throw new Exception();
                    }
                }
            }
        }
    }
}