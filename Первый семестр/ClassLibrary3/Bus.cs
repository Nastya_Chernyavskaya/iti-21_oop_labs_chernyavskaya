﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab8
{
    public class Bus : GroundTransport
    {
        public int emptySeats;

        public Bus()
        {
            type = TransportType.GroundTransportation;
            ticketPrice = new float[2];
        }

        public override string GetInfo()
        {
            return "Автобус. Цена на мягкий: " + ticketPrice[0] + " цена на жёсткий: " + ticketPrice[1] + base.GetInfo();
        }

        public float this[string kind]
        {
            get
            {
                switch (kind)
                {
                    case "Мягкий":
                        return ticketPrice[0];
                    case "Жёсткий":
                        return ticketPrice[1];
                    default:
                        throw new Exception();
                }
            }
            set
            {
                switch (kind)
                {
                    case "Мягкий":
                        ticketPrice[0] = value;
                        break;
                    case "Жёсткий":
                        ticketPrice[1] = value;
                        break;
                    default:
                        throw new Exception("Тип мест не обнаружен!");
                }
            }
        }

        public override int GetEmptySeats()
        {
            return emptySeats;
        }
    }
}
