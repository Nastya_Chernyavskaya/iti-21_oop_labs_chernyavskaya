﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MathArrayLib;
using System;

namespace Test_lab2
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            double[] num = new double[] { 4, 6, 8, 10 };
            ArrProcesser x = new ArrProcesser(num);
            ArrProcesser actual = x / 2;

            double[] actualValues = new double[actual.Lenght];
            for (int i = 0; i < x.Lenght; i++)
            {
                actualValues[i] = actual[i];
            }
       
            CollectionAssert.AreEqual(actualValues, new double[] { 2, 3, 4, 5 });
        }

        [TestMethod]
        public void TestMethod2()
        {
            double[] num = new double[] { 4, 6, 8, 10 };
            ArrProcesser x = new ArrProcesser(num);
            ArrProcesser actual = x * 2;

            double[] actualValues = new double[actual.Lenght];
            for (int i = 0; i < x.Lenght; i++)
            {
                actualValues[i] = actual[i];
            }

            CollectionAssert.AreEqual(actualValues, new double[] { 8, 12, 16, 20 });
        }

        [TestMethod]
        public void TestMethod3()
        {
            double[] num = new double[] { 4, 6, 8, 10 };
            ArrProcesser x1 = new ArrProcesser(num);
            ArrProcesser x2 = new ArrProcesser(num);
            ArrProcesser actual = x1 + x2;

            double[] actualValues = new double[actual.Lenght];
            for (int i = 0; i < actual.Lenght; i++)
            {
                actualValues[i] = actual[i];
            }

            CollectionAssert.AreEqual(actualValues, new double[] { 4, 6, 8, 10, 4, 6, 8, 10 });
        }

        [TestMethod]
        public void TestMethod4()
        {
            double[] num = new double[] { 0, -1, -2, 10 };
            ArrProcesser x = new ArrProcesser(num);
            ArrProcesser actual = x / 2;

            double[] actualValues = new double[actual.Lenght];
            for (int i = 0; i < x.Lenght; i++)
            {
                actualValues[i] = actual[i];
            }

            CollectionAssert.AreEqual(actualValues, new double[] { 0, -0.5, -1, 5 });
        }

        [TestMethod]
        public void TestMethod5()
        {
            double[] num = new double[] { 6, 8, 0, 5 };
            ArrProcesser x = new ArrProcesser(num);
            ArrProcesser actual = x * (-2);

            double[] actualValues = new double[actual.Lenght];
            for (int i = 0; i < x.Lenght; i++)
            {
                actualValues[i] = actual[i];
            }

            CollectionAssert.AreEqual(actualValues, new double[] { -12, -16, 0, -10 });
        }

        [TestMethod]
        public void TestMethod6()
        {
            double[] num = new double[] { 0, -1, -2, 10 };
            ArrProcesser x = new ArrProcesser(num);
            ArrProcesser actual = x / 0;

            double[] actualValues = new double[actual.Lenght];
            for (int i = 0; i < x.Lenght; i++)
            {
                actualValues[i] = actual[i];
            }

            CollectionAssert.AreEqual(actualValues, new double[] { 0, -0.5, -1, 5 });
        }
        //[TestMethod]
        //public void TestMethod2()
        //{
        //    float[] arr = new float[] { 4, 6, 8, 10 };
        //    arrProcesser x = new arrProcesser(arr);
        //    CollectionAssert.AreEqual((x + new arrProcesser(new float[] { 3, 3, 3 })).Array, new float[] { 4, 6, 8, 10, 3, 3, 3 });
        //}

        //[TestMethod]
        //public void TestMethod3()
        //{
        //    float[] arr = new float[] { 4, 6, 8, 10 };
        //    arrProcesser x = new arrProcesser(arr);
        //    CollectionAssert.AreEqual((x * 2).Array, new float[] { 8, 12, 16, 20 });
        //}

        //[TestMethod]
        //public void TestMethod4()
        //{
        //    float[] arr = new float[] { -1, 0, 8, 15 };
        //    arrProcesser x = new arrProcesser(arr);
        //    CollectionAssert.AreEqual((x / (float)0.5).Array, new float[] { -2, 0, 16, 30 });
        //}

        //[TestMethod]
        //public void TestMethod5()
        //{
        //    float[] arr = new float[] { -1, 0, 8, 15 };
        //    arrProcesser x = new arrProcesser(arr);
        //    CollectionAssert.AreEqual((x * (float)0.5).Array, new float[] { (float)-0.5, 0, 4, (float)7.5 });
        //}

        //[TestMethod]
        //public void TestMethod6()
        //{
        //    float[] arr = new float[] { -1, 0, 8, 15 };
        //    arrProcesser x = new arrProcesser(arr);
        //    CollectionAssert.AreEqual((x + new arrProcesser(new float[] { 0, -1, (float)-0.5 })).Array, new float[] { -1, 0, 8, 15, 0, -1, (float)-0.5 });
        //}

        //[TestMethod]
        //public void TestMethod7()
        //{
        //    float[] arr = new float[] { -1, 0, (float)0.8, 15 };
        //    arrProcesser x = new arrProcesser(arr);
        //    CollectionAssert.AreEqual((x / (float)-0.5).Array, new float[] { 2, 0, (float)-1.6, -30 });
        //}

        //[TestMethod]
        //public void TestMethod8()
        //{
        //    float[] arr = new float[] { -1, 0, (float)0.8, 15 };
        //    arrProcesser x = new arrProcesser(arr);
        //    CollectionAssert.AreEqual((x * (float)-0.5).Array, new float[] { (float)0.5, 0, (float)-0.4, (float)-7.5 });
        //}

        //[TestMethod]
        //public void TestMethod9()
        //{
        //    float[] arr = new float[] { -1, 0, (float)0.8, 15 };
        //    arrProcesser x = new arrProcesser(arr);
        //    CollectionAssert.AreEqual((x + new arrProcesser(new float[] { 0, 2, (float)-0.5, -5 })).Array, new float[] { -1, 0, (float)0.8, 15, 0, 2, (float)-0.5, -5 });
        //}

        //[TestMethod]
        //public void TestMethod10()
        //{
        //    float[] arr = new float[] { 1, 8, 12, 4 };
        //    arrProcesser x = new arrProcesser(arr);
        //    CollectionAssert.AreEqual((x / 1).Array, new float[] { 1, 8, 12, 4 });
        //}

        //[TestMethod]
        //public void TestMethod11()
        //{
        //    float[] arr = new float[] { 1, 8, 12, 4 };
        //    arrProcesser x = new arrProcesser(arr);
        //    CollectionAssert.AreEqual((x * 0).Array, new float[] { 0, 0, 0, 0 });
        //}

        //[TestMethod]
        //public void TestMethod12()
        //{
        //    float[] arr = new float[] { 1, 8, 12, 4 };
        //    arrProcesser x = new arrProcesser(arr);
        //    CollectionAssert.AreEqual((x + new arrProcesser(new float[] { 0, 0, 0, 0 })).Array, new float[] { 1, 8, 12, 4, 0, 0, 0, 0 });
        //}

        //[TestMethod]
        //public void TestMethod13()
        //{
        //    float[] arr = new float[] {-1, -34, (float)-0.8, -6};
        //    arrProcesser x = new arrProcesser(arr);
        //    CollectionAssert.AreEqual((x / -1).Array, new float[] { 1, 34, (float)0.8, 6 });
        //}

        //[TestMethod]
        //public void TestMethod14()
        //{
        //    float[] arr = new float[] {-1, -34, (float)-0.8, -6};
        //    arrProcesser x = new arrProcesser(arr);
        //    CollectionAssert.AreEqual((x * 1).Array, new float[] { -1, -34, (float)-0.8, -6 });
        //}

        //[TestMethod]
        //public void TestMethod15()
        //{
        //    float[] arr = new float[] {-1, -34, (float)-0.8, -6};
        //    arrProcesser x = new arrProcesser(arr);
        //    CollectionAssert.AreEqual((x + new arrProcesser(new float[] { -1, -34, (float)-0.8, -6 })).Array, new float[] { -1, -34, (float)-0.8, -6, -1, -34, (float)-0.8, -6 });
        //}
    }
}

