﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Report_Lib
{
    public class Report : IComparable
    {
        public string Number { get; private set; }
        public string Country { get; private set; }

        public Report( StringBuilder str)
        {
            Regex regex = new Regex(@"(?<country>[a-z]{2,3}) (?<number>[a-z]{2,3}\d{4})", RegexOptions.IgnoreCase);
            var tmp = regex.Match(str.ToString());
            if (!tmp.Success)
            {
                throw new Exception("Неверный формат");
            }
            Country = tmp.Groups["country"].Value;
            Number = tmp.Groups["number"].Value;

        }

        public int CompareTo(object obj)
        {
            return Country.CompareTo(((Report)obj).Country);
        }
    }
}
