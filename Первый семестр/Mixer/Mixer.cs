﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixerLib
{
    public class Mixer
    {
        public string[] Mix(string str)
        {
            string[] words = str.Split('_');

            if (words.Length > 1)
            {
                List<string> list = new List<string>();
                Permute(words, 0, words.Length - 1, list);
                return list.ToArray();
            }
            return words;
        }

        void Permute(string[] strs, int l, int r, List<string> lst)
        {
            if (l == r)
            {
                string tmp = "";
                foreach (var item in strs)
                    tmp += item + ' ';
                tmp.Remove(tmp.Length - 1);
                lst.Add(tmp);
            }
            else
            {
                for (int i = l; i <= r; i++)
                {
                    strs = Swap(strs, l, i);
                    Permute(strs, l + 1, r, lst);
                    strs = Swap(strs, l, i);
                }
            }
        }

        string[] Swap(string[] a, int i, int j)
        {
            string temp = a[i];
            a[i] = a[j];
            a[j] = temp;
            return a;
        }

        public StringBuilder[] FindWord(string text, string word)
        {
            string[] sents = text.Split(new char[] { '.', '!', '?' });
            List<StringBuilder> answer = new List<StringBuilder>();
            foreach (var item in sents)
            {
                if (item.Contains(word))
                {
                    answer.Add(new StringBuilder(item));
                }
            }
            return answer.ToArray();
        }
    }
}


//public string[] Mix(string str)
//{
//    string[] words = str.Split('_');

//    if (words.Length > 1)
//    {
//        List<string> list = new List<string>();
//        Permute(words, 0, 2, list);
//        return list.ToArray();
//    }
//    return words;
//}

//void Permute(string[] strs, int l, int r, List<string> lst)
//{
//    if (l == r)
//    {
//        string tmp = "";
//        foreach (var item in strs)
//            tmp += item + ' ';
//        tmp.Remove(tmp.Length - 1);
//        lst.Add(tmp);
//    }
//    else
//    {
//        for (int i = l; i <= r; i++)
//        {
//            strs = Swap(strs, l, i);
//            Permute(strs, l + 1, r, lst);
//            strs = Swap(strs, l, i);
//        }
//    }
//}

//string[] Swap(string[] a, int i, int j)
//{
//    string temp = a[i];
//    a[i] = a[j];
//    a[j] = temp;
//    return a;
//}