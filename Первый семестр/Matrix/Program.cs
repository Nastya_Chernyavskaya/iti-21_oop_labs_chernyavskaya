﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MatrixLib;

namespace Laba3
{
    class Program
    {
        static void Main(string[] args)
        {
            Matrix Input()
            {
                uint n;
                uint m;

                Console.WriteLine("Введите количество строк матрицы");

                while (!uint.TryParse(Console.ReadLine(), out n))
                {
                    try
                    {
                        throw new ArgumentException();
                    }
                    catch (ArgumentException ex)
                    {
                        Console.WriteLine("Ошибка: " + ex.Message);
                    }                 
                }
  
                Console.WriteLine("Введите количество столбцов матрицы");

                while (!uint.TryParse(Console.ReadLine(), out m))
                {
                    try
                    {
                        throw new ArgumentException(); 
                    }
                    catch (ArgumentException ex)
                    {
                        Console.WriteLine("Ошибка: " + ex.Message);
                    }
                }

                Matrix matrix = new Matrix(n, m);

                Console.WriteLine("Введите элементы матрицы");

                for (int i = 0; i < n; i++)
                {
                    for (int j = 0; j < m; j++)
                    {
                        Console.Write("Элемент матрицы [{0}, {1}] = ", i , j );
                        matrix[i, j] = Single.Parse(Console.ReadLine());
                    }
                }
                return matrix;
            }

            void OutputMtrx(Matrix matrix)
            {
                for (int i = 0; i < matrix.Row; i++)
                {
                    for (int j = 0; j < matrix.Col; j++)
                    {
                        Console.Write("{0} ", matrix[i, j]);
                    }
                    Console.WriteLine();
                }                
            }

            int choice;

            input:

            Matrix mtrxA = Input();
            Matrix mtrxB = Input();
            Matrix mtrxC = Input();

            do
            {
                Console.WriteLine("-----------------------------------------------------------------------");
                Console.WriteLine("              Меню              ");
                Console.WriteLine("Выберите пункт меню:");
                Console.WriteLine("1:Создание матриц A, B и C;");
                Console.WriteLine("2:Ввывод матриц A, B и C;");
                Console.WriteLine("3:Вычисление суммы квадратов отрицательных элементов каждой матрицы;");
                Console.WriteLine("4:Вычисление A-B и B-A-C, если это возможно;");
                Console.WriteLine("5:Если A <= B <= C, заменить все отрицательные элементы матриц A и B\nна значение сумммы квадратов положительных элементов расположенных ниже минимального\nсреди элементов строк с номерами кратными 3 в матрице C;");
                Console.WriteLine("6:Выход;");
                Console.WriteLine("----------------------------------------------------------------------");

                while (!Int32.TryParse(Console.ReadLine(), out choice))
                {
                    Console.WriteLine("Неверный ввод!");
                }

                Console.WriteLine();

                switch (choice)
                {
                    case 1:

                        goto input;

                    case 2:

                        Console.WriteLine("Матрица A");
                        OutputMtrx(mtrxA);

                        Console.WriteLine("Матрица B");
                        OutputMtrx(mtrxB);

                        Console.WriteLine("Матрица C");
                        OutputMtrx(mtrxC);

                        break;

                    case 3:

                        float summaA;
                        float summaB;
                        float summaC;

                        summaA = mtrxA.SumSquaresElements();
                        Console.WriteLine("Cумма квадратов отрицательных элементов матрицы А: {0:0.##}", summaA);

                        summaB = mtrxB.SumSquaresElements();
                        Console.WriteLine("Cумма квадратов отрицательных элементов матрицы B:{0:0.##}", summaB);

                        summaC = mtrxC.SumSquaresElements();
                        Console.WriteLine("Cумма квадратов отрицательных элементов матрицы C:{0:0.##}", summaC);

                        break;

                    case 4:

                        Matrix mtrxA_B = mtrxA - mtrxB;

                        for (int i = 0; i < mtrxA.Row; i++)
                        {
                            for (int j = 0; j < mtrxA.Col; j++)
                            {
                                Console.WriteLine("Результат A-B [" + i + "][" + j + "]=" + mtrxA_B[i, j]);
                            }
                        }

                        Matrix mtrxA_B_C = mtrxA - mtrxB - mtrxC;

                        for (int i = 0; i < mtrxA.Row; i++)
                        {
                            for (int j = 0; j < mtrxA.Col; j++)
                            {
                                Console.WriteLine("Результат B-A-C [" + (i + 1) + "][" + (j + 1) + "]=" + mtrxA_B_C[i, j]);
                            }
                        }

                        break;

                    case 5:

                        if ((mtrxB >= mtrxA) && (mtrxB <= mtrxC))
                        {
                            int mtrxC_i = mtrxC.SearchMinElement(2);
                            float sum = Matrix.Sum_El_C(mtrxC_i, mtrxC);

                            Console.WriteLine("Матрица A");
                            Matrix.Res(mtrxA, sum);
                            OutputMtrx(mtrxA);

                            Console.WriteLine("Матрица B");
                            Matrix.Res(mtrxB, sum);
                            OutputMtrx(mtrxB);
                        }
                        else
                        {
                            Console.WriteLine("Условие не выполняется");
                            break;
                        }

                        break;
                } 
            } while (choice != 6);

        }

    }
}





