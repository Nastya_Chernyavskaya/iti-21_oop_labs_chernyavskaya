﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicketsData
{
    public class TicketsDataLib
    {
        List<Ticket> tickets;

        public TicketsDataLib()
        {
            tickets = new List<Ticket>();
        }

        private class Ticket
        {
            public DateTime Time { get; private set; }
            public Genre Genre { get; private set; }
            public Ticket(DateTime time, Genre genre)
            {
                Time = time;
                Genre = genre;
            }
        }

        public void AddTicket(DateTime time, Genre genre)
        {
            tickets.Add(new Ticket(time, genre));
        }

        public DateTime[] GetTicketsByPeriod(out Genre[] genres, DateTime start, DateTime end)
        {
            List<DateTime> time = new List<DateTime>();
            List<Genre> tmpGenres = new List<Genre>();
            for (int i = 0; i < tickets.Count; i++)
            {
                if (tickets[i].Time.Date >= start.Date && tickets[i].Time.Date <= end.Date)
                {
                        time.Add(tickets[i].Time);
                        tmpGenres.Add(tickets[i].Genre);
                }
            }
            genres = tmpGenres.ToArray();
            return time.ToArray();
        }

        public int[] GetPopularityByPeriod(DateTime start, DateTime end)
        {
            int[] counts = new int[3];
            foreach (var item in tickets)
            {
                if (item.Time.Date >= start.Date && item.Time.Date <= end.Date)
                {
                    counts[(int)item.Genre]++;
                }
            }
            return counts;
        }

        public string GetMostPopularGenre(DateTime start, DateTime end)
        {
            var tmp = GetPopularityByPeriod(start, end);
            int max = Math.Max(tmp[0], Math.Max(tmp[1], tmp[2]));
            if (max == 0)
            {
                throw new Exception();
            }
            string str = "";
            for (int i = 0; i < 3; i++)
            {
                if (tmp[i] == max)
                {
                    str += (Genre)i + "\n";
                }
            }
            return str;
        }
    }

    public enum Genre
    {
        Comedy,
        Drama,
        Romantic
    }
}
