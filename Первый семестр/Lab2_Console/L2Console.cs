﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathArrayLib;
namespace Lab2_Console
{
    class L2Console
    {
        static float[] arrСreation()
        {
            Console.WriteLine("Введите количество элементов массива");
            ushort length;
            while (!ushort.TryParse(Console.ReadLine(), out length))
            {
                Console.WriteLine("Введите число!");
            }
            float[] tmp = new float[length];
            for (ushort i = 0; i < length; ++i)
            {
                Console.WriteLine($"Введите элемент [{i + 1}]");
                if (!float.TryParse(Console.ReadLine(), out float x))
                {
                    Console.WriteLine("Введите число!");
                    ++i;
                    continue;
                }
                tmp[i] = x;
            }
            return tmp;
        }

        static void Main(string[] args)
        {
            int num;

            Console.WriteLine("Создание первого массива A...");
            Console.WriteLine("Укажите длину массива А");
            num = Convert.ToInt32(Console.ReadLine());
            ArrProcesser array1 = new ArrProcesser(num);
            Console.WriteLine("Укажите элементы массива А");
            for (int i = 0; i < num; i++)
            {
                Console.Write($"A[{i}] = ");
                array1[i] = Convert.ToDouble(Console.ReadLine());
            }


            Console.WriteLine("Создание первого массива B...");
            Console.WriteLine("Укажите длину массива B");
            num = Convert.ToInt32(Console.ReadLine());
            ArrProcesser array2 = new ArrProcesser(num);
            Console.WriteLine("Укажите элементы массива B");
            for (int i = 0; i < num; i++)
            {
                Console.Write($"A[{i}] = ");
                array2[i] = Convert.ToDouble(Console.ReadLine());
            }

            Console.WriteLine($"Создано два массива\nМассив A:\n{(ArrProcesser)array1}\nМассив B:\n{(ArrProcesser)array2}");

            int choice;
            do
            {
                Console.WriteLine("-------------------------------------------------------------------");
                Console.WriteLine("              Меню              ");
                Console.WriteLine("Выберите пункт меню:");
                Console.WriteLine("1:Сумма массивов;");
                Console.WriteLine("2:Умножение на число;");
                Console.WriteLine("3:Деление на число;");
                Console.WriteLine("4:Выход;");
                Console.WriteLine("-------------------------------------------------------------------");
                while (!Int32.TryParse(Console.ReadLine(), out choice))
                {
                    Console.WriteLine("Неверный ввод!");
                }
                Console.WriteLine();

                switch (choice)
                {
                    case 1:
                        Console.WriteLine($"Массив А + массив B = {array1 + array2}");
                        break;
                    case 2:
                        Console.WriteLine("Введите число x = ");
                        float X1;
                        while (!Single.TryParse(Console.ReadLine(), out X1))
                        {
                            Console.WriteLine("Введите число!");
                        }
                        Console.WriteLine($"Массив A * x = {array1 * X1}\nMассив B * x = {array2 * X1}");
                        break;
                    case 3:
                        Console.Write("Введите чило х = ");
                        float X2;
                        while (!Single.TryParse(Console.ReadLine(), out X2))
                        {
                            Console.WriteLine("Введите число!");
                        }
                        try
                        {
                            Console.WriteLine($"Array1 / {0} = \n{array1 / 0}");
                        }
                        catch (Exception)
                        {
                            Console.WriteLine("X не может быть 0");
                        }

                        Console.WriteLine($"Массив A / x = {array1 / X2}\nМассив B / x = {array2 / X2}");
                        break;
                }
            } while (choice != 4);
        }
    }
}
