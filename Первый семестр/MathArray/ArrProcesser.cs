﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathArrayLib
{
    public class ArrProcesser
    {
        private double[] array;
        public int Lenght
        {
            get
            {
                return array.Length;
            }
        }
        

        public ArrProcesser(int size)
        {
            array = new double[size];
        }
       
        public ArrProcesser(double[] array)
        {
            this.array = new double[array.Length];
            Array.Copy(array, this.array, array.Length);
        }
        
        public double this[int index]
        {
            get
            {
                return array[index];
            }
            set
            {
                array[index] = value;
            }
        }

        public static ArrProcesser operator +(ArrProcesser arrA, ArrProcesser arrB)
        {
            double[] arrCValue = new double[arrA.Lenght + arrB.Lenght];
            for (int i = 0; i < arrA.Lenght; i++)
            {
                arrCValue[i] = arrA[i];
            }
            for (int num = arrA.Lenght, i = 0; i < arrB.Lenght; i++, num++)
            {
                arrCValue[num] = arrB[i];
            }

            return new ArrProcesser(arrCValue);
        }

        public static ArrProcesser operator *(ArrProcesser arrA, float x)
        {
            double[] arrAValue = new double[arrA.Lenght];
            for (int i = 0; i < arrA.Lenght; i++)
            {
                arrAValue[i] = arrA[i];
                arrAValue[i] *= x;
            }
            return new ArrProcesser(arrAValue);
        }

        public static ArrProcesser operator /(ArrProcesser arrA, float x)
        {
            if (x == 0)
            {
                throw new DivideByZeroException();
            }
            double[] arrAValue = new double[arrA.Lenght];
            for (int i = 0; i < arrA.Lenght; i++)
            {
                arrAValue[i] = arrA[i];
                arrAValue[i] /= x;
            }
            return new ArrProcesser(arrAValue);
        }

        public override string ToString()
        {
            string str = "";
            foreach (var item in array)
            {
                str += item + " ";
            }
            return str;
        }
    }
}
