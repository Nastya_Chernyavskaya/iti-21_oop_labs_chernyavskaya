﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TicketsData;

namespace Lab5Wpf
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            TicketsData = new TicketsDataLib();
        }

        TicketsDataLib TicketsData;

        private void button1_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex >= 0 && listBox1.SelectedIndex <= 2)
            {
                var genre = (Genre)listBox1.SelectedIndex;
                TicketsData.AddTicket(dateTimePicker1.Value, genre);
                listBox1.ClearSelected();
            }
            else
            {
                MessageBox.Show("Введены некорректные данные");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            int i = 1;
            var tmp = TicketsData.GetTicketsByPeriod(out Genre[] genres, dateTimePicker2.Value, dateTimePicker3.Value);
            string str = "";
            for (int j = 0; j < tmp.Length; j++)
            {
                str += "Билет №" + i++ + " жанр: " + genres[j] + " время: " + tmp[j].ToString() + "\n";
            }
            MessageBox.Show(str);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
                var tmp = TicketsData.GetPopularityByPeriod(dateTimePicker2.Value, dateTimePicker3.Value);
                MessageBox.Show("Comedy: " + tmp[0] + "\nDrama: " + tmp[1] + "\nRomantic: " + tmp[2]);
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                MessageBox.Show(TicketsData.GetMostPopularGenre(dateTimePicker2.Value, dateTimePicker3.Value));
            }
            catch
            {
                MessageBox.Show("За данный период не было посещений");
            }
        }
    }
}
