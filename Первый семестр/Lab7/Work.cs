﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab7
{
    public abstract class Work
    {
        public DateTime Time { get; private set; }
        public float Price { get; private set; }
        public string CarModel { get; private set; }
        public Work(DateTime Time, float Price, string CarModel)
        { 
            this.Time = Time;
            this.Price = Price;
            this.CarModel = CarModel;
        }
    }
}
