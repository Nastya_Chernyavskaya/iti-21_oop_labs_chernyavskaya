﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab7
{
    public class WorkReports
    {
        List<Work> list;
        public WorkReports()
        {
            list = new List<Work>();
        }
        public void AddWork(Work work)
        {
            list.Add(work);
        }

        public string GetWorksCountByModel(Type type)
        {
            Dictionary<string, int> modelsInfo = new Dictionary<string, int>();
            foreach (var item in list)
            {
                if (type.IsInstanceOfType(item))
                {
                    if (modelsInfo.ContainsKey(item.CarModel))
                    {
                        modelsInfo[item.CarModel]++;
                    }
                    else
                    {
                        modelsInfo.Add(item.CarModel, 1);
                    }
                }
            }
            string str = "";
            foreach (var item in modelsInfo)
            {
                str += item.Key + ": " + item.Value + "\n";
            }
            return str;
        }

        public string GetMostPopularWorkbyModel(string model)
        {
            Dictionary<Type, int> modelInfo = new Dictionary<Type, int>();
            foreach (var item in list)
            {
                if (item.CarModel == model)
                {
                    if (modelInfo.ContainsKey(item.GetType()))
                    {
                        modelInfo[item.GetType()]++;
                    }
                    else
                    {
                        modelInfo.Add(item.GetType(), 1);
                    }
                }
            }
            int max = -1;
            string type = null;
            foreach (var item in modelInfo)
            {
                if (item.Value > max)
                {
                    max = item.Value;
                    type = item.Key.Name;
                }
            }

            return type;
        }

        public float GetCostByPeriod(Type type, DateTime start, DateTime end)
        {
            float cost = 0;
            foreach (var item in list)
            {
                if (item.Time.Date >= start.Date && item.Time.Date <= end.Date && type.IsInstanceOfType(item))
                {
                    cost += item.Price;
                }
            }
            return cost;
        }

        public string GetAllReports()
        {
            string str = "";
            foreach (var item in list)
            {
                str += item.CarModel + " " + item.GetType().Name + " " + item.Price + " " + item.Time.ToString("dd.MM.yyyy") + "\n";
            }
            return str;
        }
    }

}
