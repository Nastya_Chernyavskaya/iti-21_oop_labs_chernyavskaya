﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MixerLib;

namespace ConsoleLab4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите предложение, слова в котором разделены _.");
            Mixer mixer = new Mixer();
            foreach (var item in mixer.Mix(Console.ReadLine()))
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("=======================");
            Console.WriteLine("Введите текст");
            string text = Console.ReadLine();
            Console.WriteLine("Введите слово");
            string word = Console.ReadLine();
            Console.WriteLine();
            foreach (var item in mixer.FindWord(text, word))
            {
                Console.WriteLine(item);
            }
            Console.ReadKey();
        }
    }
}
