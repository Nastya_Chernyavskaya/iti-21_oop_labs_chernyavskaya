﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ClassLibrary1
{/// <summary>
/// пространство имён - контейнер классов
/// </summary>
    public class CurvedTrapezoid
    {/// <summary>
    /// Класс с методами для вычисления параметров трапеции
    /// </summary>
        public double UpperLimit { get; private set; }
        /// <summary>
        /// Метод управления вводом для UpperLimit
        /// </summary>
        public double LowerLimit { get; private set; }
        /// <summary>
        /// Метод управления вводом для LowerLimit
        /// </summary>
        public double Koef { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="upperLimit"></param>
        /// <param name="lowerLimit"></param>
        /// <param name="koef"></param>
        public CurvedTrapezoid(double upperLimit, double lowerLimit, double koef)
        {
            UpperLimit = upperLimit;
            LowerLimit = lowerLimit;
            Koef = koef;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private double Sqare()
        {
            return Math.Round(Math.Abs(Koef * Math.Log(Math.Abs(UpperLimit)) - Koef * Math.Log(Math.Abs(LowerLimit))), 3);
        }
        private double ALength()
        {
            return Math.Round(Math.Abs(Koef / UpperLimit),3);
        }

        private double BLength()
        {
            return Math.Round(Math.Abs(Koef / LowerLimit), 3);
        }

        private double CLength()
        {
            return Math.Abs(UpperLimit - LowerLimit);
        }

        private double DLength()
        {
            double res = 0, step = 0.1;
            double x = LowerLimit;

            while (x < UpperLimit)
            {
                res += Math.Sqrt(Math.Pow(step, 2) + Math.Pow(Koef / (x + step) - Koef / (x + step), 2));
                x += step;
            }

            return Math.Round(Math.Abs(res), 3);
        }
        private double Perimeter()
        {
            return ALength() + BLength() + CLength() + DLength();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="X"></param>
        /// <param name="Y"></param>
        /// <returns></returns>
        public string IsPointLocation(double X, double Y)
        {
            string pointLocation;

            if ((Y > 0 && X > 0 && X < UpperLimit && X < LowerLimit && Y < (Koef/X)) || (Y < 0 && X < 0 && X > LowerLimit && X < LowerLimit && Y > (Koef/X)))
            {
                pointLocation = "Точка расположена внутри фигуры";
            }
            else 
                if (((Y == 0 && X >= LowerLimit && X <= UpperLimit) || (X == LowerLimit && Y <= (Koef/X) && Y >= 0) || (X == UpperLimit && Y <= (Koef/X) && Y >= 0) || (X <= UpperLimit && X >= LowerLimit && Y >= 0 && Y == (Koef/X))) || ((Y == 0 && X >= LowerLimit && X <= UpperLimit) || (X == LowerLimit && Y >= (Koef / X) && Y <= 0) || (X == UpperLimit && Y >= (Koef / X) && Y <= 0) || (X <= UpperLimit && X >= LowerLimit && Y <= 0 && Y == (Koef / X))))
                {
                    pointLocation = "Точка расположена на границе фигуры";
                }
            else
            {
                pointLocation = "Точка расположена вне фигуры";
            }

            return pointLocation;
        }
        /// <summary>
        /// Метод вывода параметров 
        /// </summary>
        /// <returns></returns>
        public string GetResults()
        {
            return "Площад S = " + Convert.ToString(Sqare()) + "\n"
                + "Длина стороны A = " + Convert.ToString(ALength()) + "\n"
                + "Длина стороны В = " + Convert.ToString(BLength()) + "\n"
                + "Длина стороны С = " + Convert.ToString(CLength()) + "\n"
                + "Длина стороны D = " + Convert.ToString(DLength()) + "\n"
                + "Периметр P = " + Convert.ToString(Perimeter());
        }
    }
}
