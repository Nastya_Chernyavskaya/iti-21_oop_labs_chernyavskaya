﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Lab7;

namespace WindowsFormsApp7
{
    public partial class Form1 : Form
    {
        WorkReports wReps;

        public Form1()
        {
            InitializeComponent();
            wReps = new WorkReports();
        }

        void Clear()
        {
            listBox1.ClearSelected();
            textBox1.Text = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Work tmp;
            switch (listBox1.SelectedIndex)
            {
                case 0:
                    tmp = new TireChange(dateTimePicker3.Value, 10, textBox1.Text);
                    break;
                case 1:
                    tmp = new PunctureRepair(dateTimePicker3.Value, 40, textBox1.Text);
                    break;
                case 2:
                    tmp = new WheelBalancing(dateTimePicker3.Value, 8, textBox1.Text);
                    break;
                case 3:
                    tmp = new TCOC(dateTimePicker3.Value, 25, textBox1.Text);
                    break;
                default:
                    MessageBox.Show("Некорректные данные");
                    return;
            }
            if (textBox1.Text != "")
            {
                wReps.AddWork(tmp);
                Clear();
            }
            else
            {
                MessageBox.Show("Некорректные данные");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show(wReps.GetAllReports());
            Clear();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Type type;
            switch (listBox1.SelectedIndex)
            {
                case 0:
                    type = typeof(TireChange);
                    break;
                case 1:
                    type = typeof(PunctureRepair);
                    break;
                case 2:
                    type = typeof(WheelBalancing);
                    break;
                case 3:
                    type = typeof(TCOC);
                    break;
                default:
                    MessageBox.Show("Некорректные данные");
                    return;
            }
            MessageBox.Show(wReps.GetWorksCountByModel(type));
            Clear();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                MessageBox.Show(wReps.GetMostPopularWorkbyModel(textBox1.Text));
                Clear();
            }
            else
            {
                MessageBox.Show("Некорректные данные");
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Type type;
            switch (listBox1.SelectedIndex)
            {
                case 0:
                    type = typeof(TireChange);
                    break;
                case 1:
                    type = typeof(PunctureRepair);
                    break;
                case 2:
                    type = typeof(WheelBalancing);
                    break;
                case 3:
                    type = typeof(TCOC);
                    break;
                default:
                    MessageBox.Show("Некорректные данные");
                    return;
            }
            MessageBox.Show(wReps.GetCostByPeriod(type, dateTimePicker1.Value, dateTimePicker2.Value).ToString());
            Clear();
        }
    }
}
