﻿using System;

namespace ViolationsCRUD
{
    public class Violation
    {
        public int Id { get; set; }

        public int AutoNumber { get; set; }

        public DateTime Date { get; set; }

        public ViolationType Type { get; set; }

        public decimal FineAmount { get; set; }

        public int DriverId { get; set; }

        public Inspector Inspector { get; set; }

        public override string ToString()
        {
            return string.Format($"Auto number: {AutoNumber} Date: {Date} Violation type: {Type} Fine amount: {FineAmount} " +
                $"Driver id: {DriverId} Inspector: {Inspector.FullName}");
        }
    }
}
