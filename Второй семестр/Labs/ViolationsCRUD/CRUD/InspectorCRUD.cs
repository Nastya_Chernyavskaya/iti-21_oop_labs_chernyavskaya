﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;

namespace ViolationsCRUD
{
    public static class InspectorCRUD
    {
        public static void Create(Inspector item)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("insert into Inspectors values (@name); select cast(scope_identity() AS int)", connection);
                command.Parameters.AddWithValue("@name", item.FullName);
                item.Id = (int)command.ExecuteScalar();
            }
        }

        public static Inspector Get(int id)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("select * from Inspectors where Id = @id", connection);
                command.Parameters.AddWithValue("@id", id);
                var reader = command.ExecuteReader();
                Inspector answ = null;
                if (reader.Read())
                {
                    answ = new Inspector();
                    answ.Id = id;
                    answ.FullName = reader.GetString(1);
                }
                return answ;
            }
        }

        public static Inspector Get(string name)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("select * from Inspectors where FullName = @name", connection);
                command.Parameters.AddWithValue("@name", name);
                var reader = command.ExecuteReader();
                Inspector answ = null;
                if (reader.Read())
                {
                    answ = new Inspector();
                    answ.Id = reader.GetInt32(0);
                    answ.FullName = name;
                }
                return answ;
            }
        }

        public static List<Inspector> GetAll()
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("select * from Inspectors", connection);
                var reader = command.ExecuteReader();
                List<Inspector> answ = new List<Inspector>();
                while (reader.Read())
                {
                    answ.Add(new Inspector()
                    {
                        Id = reader.GetInt32(0),
                        FullName = reader.GetString(1)
                    });
                }
                return answ;
            }
        }

        public static void Update(Inspector item)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("update Inspectors set FullName = @name where Id = @id", connection);
                command.Parameters.AddWithValue("@id", item.Id);
                command.Parameters.AddWithValue("@name", item.FullName);
                command.ExecuteNonQuery();
            }
        }

        public static void Delete(Inspector item)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("delete from Inspectors where Id = @id", connection);
                command.Parameters.AddWithValue("@id", item.Id);
                command.ExecuteNonQuery();
                item.Id = -1;
            }
        }
    }
}
