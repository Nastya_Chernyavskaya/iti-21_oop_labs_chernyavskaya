﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;

namespace ViolationsCRUD
{
    public static class ViolationTypeCRUD
    {
        public static void Create(ViolationType item)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("insert into ViolationTypes values (@type); select cast(scope_identity() AS int)", connection);
                command.Parameters.AddWithValue("@type", item.Name);
                item.Id = (int)command.ExecuteScalar();
            }
        }

        public static ViolationType Get(int id)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("select * from ViolationTypes where Id = @id", connection);
                command.Parameters.AddWithValue("@id", id);
                var reader = command.ExecuteReader();
                ViolationType answ = null;
                if (reader.Read())
                {
                    answ = new ViolationType();
                    answ.Id = id;
                    answ.Name = reader.GetString(1);
                }
                return answ;
            }
        }

        public static ViolationType Get(string name)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("select * from ViolationTypes where Type = @name", connection);
                command.Parameters.AddWithValue("@name", name);
                var reader = command.ExecuteReader();
                ViolationType answ = null;
                if (reader.Read())
                {
                    answ = new ViolationType();
                    answ.Id = reader.GetInt32(0);
                    answ.Name = name;
                }
                return answ;
            }
        }

        public static List<ViolationType> GetAll()
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("select * from ViolationTypes", connection);
                var reader = command.ExecuteReader();
                List<ViolationType> answ = new List<ViolationType>();
                while (reader.Read())
                {
                    answ.Add(new ViolationType() {
                        Id = reader.GetInt32(0),
                        Name = reader.GetString(1)
                    });
                }
                return answ;
            }
        }

        public static void Update(ViolationType item)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("update ViolationTypes set Type = @name where Id = @id", connection);
                command.Parameters.AddWithValue("@id", item.Id);
                command.Parameters.AddWithValue("@name", item.Name);
                command.ExecuteNonQuery();
            }
        }

        public static void Delete(ViolationType item)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("delete from ViolationTypes where Id = @id", connection);
                command.Parameters.AddWithValue("@id", item.Id);
                command.ExecuteNonQuery();
                item.Id = -1;
            }
        }
    }
}
