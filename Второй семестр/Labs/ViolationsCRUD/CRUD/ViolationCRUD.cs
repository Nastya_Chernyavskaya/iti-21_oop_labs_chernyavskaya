﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;

namespace ViolationsCRUD
{
    public class ViolationCRUD
    {
        public static void Create(Violation item)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("insert into Violations values (@autoNum, @date, @typeId, @fineAmount, @driverId, @inspectroId);" +
                    "select cast(scope_identity() AS int)", connection);

                if (item.Type.Id < 0)
                {
                    ViolationTypeCRUD.Create(item.Type);
                }
                if (item.Inspector.Id < 0)
                {
                    InspectorCRUD.Create(item.Inspector);
                }

                command.Parameters.AddWithValue("@autoNum", item.AutoNumber);
                command.Parameters.AddWithValue("@date", item.Date);
                command.Parameters.AddWithValue("@typeId", item.Type.Id);
                command.Parameters.AddWithValue("@fineAmount", item.FineAmount);
                command.Parameters.AddWithValue("@driverId", item.DriverId);
                command.Parameters.AddWithValue("@inspectroId", item.Inspector.Id);
                item.Id = (int)command.ExecuteScalar();
            }
        }

        public static Violation Get(int id)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("select * from Violations where Id = @id", connection);
                command.Parameters.AddWithValue("@id", id);
                var reader = command.ExecuteReader();
                Violation answ = null;
                if (reader.Read())
                {
                    answ = new Violation();
                    answ.Id = id;
                    answ.AutoNumber = reader.GetInt32(1);
                    answ.Date = reader.GetDateTime(2);
                    answ.Type = ViolationTypeCRUD.Get(reader.GetInt32(3));
                    answ.FineAmount = reader.GetDecimal(4);
                    answ.DriverId = reader.GetInt32(5);
                    answ.Inspector = InspectorCRUD.Get(reader.GetInt32(6));
                }
                return answ;
            }
        }

        public static List<Violation> GetAll()
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("select * from Violations", connection);
                var reader = command.ExecuteReader();
                List<Violation> answ = new List<Violation>();
                while (reader.Read())
                {
                    answ.Add(new Violation()
                    {
                        Id = reader.GetInt32(0),
                        AutoNumber = reader.GetInt32(1),
                        Date = reader.GetDateTime(2),
                        Type = ViolationTypeCRUD.Get(reader.GetInt32(3)),
                        FineAmount = reader.GetDecimal(4),
                        DriverId = reader.GetInt32(5),
                        Inspector = InspectorCRUD.Get(reader.GetInt32(6))
                    });
                }
                return answ;
            }
        }

        public static void Update(Violation item)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("update Violations set AutoNumber = @num, Date = @date, ViolationTypeId = @vId, " +
                    "FineAmount = @fAmount, DriverId = @dId, InspectorId = @iId where Id = @id", connection);

                if (item.Type.Id < 0)
                {
                    ViolationTypeCRUD.Create(item.Type);
                }
                if (item.Inspector.Id < 0)
                {
                    InspectorCRUD.Create(item.Inspector);
                }

                command.Parameters.AddWithValue("@id", item.Id);
                command.Parameters.AddWithValue("@num", item.AutoNumber);
                command.Parameters.AddWithValue("@date", item.Date);
                command.Parameters.AddWithValue("@vId", item.Type.Id);
                command.Parameters.AddWithValue("@fAmount", item.FineAmount);
                command.Parameters.AddWithValue("@dId", item.DriverId);
                command.Parameters.AddWithValue("@iId", item.Inspector.Id);
                command.ExecuteNonQuery();
            }
        }

        public static void Delete(Violation item)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("delete from Violations where Id = @id", connection);
                command.Parameters.AddWithValue("@id", item.Id);
                command.ExecuteNonQuery();
                item.Id = -1;
            }
        }
    }
}