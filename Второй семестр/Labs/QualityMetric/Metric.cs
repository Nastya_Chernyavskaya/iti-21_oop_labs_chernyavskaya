﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QualityMetric
{
    public class Metric
    {
        private bool[] expect;
        private bool[] real;
        public Metric(bool[] expect, bool[] real)
        {
            this.expect = expect;
            this.real = real;
        }

        public int CounterTruePositive()
        {
            if (expect.Length != real.Length)
            {
                throw new Exception();
            }
            int count = 0;
            for (int i = 0; i < expect.Length; i++)
            {
                if (expect[i] == true && real[i] == true)
                {
                    count++;
                }
            }
            return count;
        }

        public int CounterFalsePositive()
        {
            if (expect.Length != real.Length)
            {
                throw new Exception();
            }
            int count = 0;
            for (int i = 0; i < expect.Length; i++)
            {
                if (expect[i] == false && real[i] == true)
                {
                    count++;
                }
            }
            return count;
        }
    }
}
