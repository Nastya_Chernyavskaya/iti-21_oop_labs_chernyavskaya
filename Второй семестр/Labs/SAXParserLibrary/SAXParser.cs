﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;

namespace SAXParserLibrary
{
    public class SAXParser 
    {
        public string Path { get; private set; }
        public SAXParser (string path)
        {
            Path = path;
        }

        public List<Violations> ParseXML()
        {
            CheckXML();
            List<Violations> violations = new List<Violations>();
            using (XmlReader xr = XmlReader.Create(Path))
            {
                //CheckXML();
                int id = default;
                string name = default;
                string avtoNumber = default;
                DateTime violationDate = default;
                string idNumber = default;
                decimal fine = default;
                Violations.ViolationTypes violationType = default;
                string element = default;

                while (xr.Read())
                {
                    // reads the element
                    if (xr.NodeType == XmlNodeType.Element)
                    {
                        element = xr.Name; // the name of the current element
                        //if (element == "violation")
                        //{
                        //    MessageBox.Show(xr.GetAttribute("place"));
                        //}
                    }
                    // reads the element value
                    else if (xr.NodeType == XmlNodeType.Text)
                    {
                        switch (element)
                        {
                            case "id":
                                id = Convert.ToInt32(xr.Value);
                                break;
                            case "name":
                                name = xr.Value;
                                break;
                            case "avtoNumber":
                                avtoNumber = xr.Value;
                                break;
                            case "violationDate":
                                violationDate = DateTime.Parse(xr.Value);
                                break;
                            case "idNumber":
                                idNumber = xr.Value;
                                break;
                            case "fine":
                                fine = decimal.Parse(xr.Value);
                                break;
                            case "violationType":
                                violationType = (Violations.ViolationTypes)Enum.Parse(typeof(Violations.ViolationTypes), xr.Value);
                                break;
                        }
                    }

                    else if ((xr.NodeType == XmlNodeType.EndElement) && (xr.Name == "violation"))
                    {
                        violations.Add(new Violations(id, name, avtoNumber, violationDate, idNumber, fine, violationType));
                    }
                }
            }
            CheckViolations(violations);
            return violations;
        }

        public void SaveXML(List<Violations> violations)
        {
            CheckViolations(violations);
            using (XmlWriter xmlWriter = XmlWriter.Create(Path))
            {
                xmlWriter.WriteStartDocument();
                xmlWriter.WriteStartElement("violations");
                foreach (Violations violation in violations)
                {
                    xmlWriter.WriteStartElement("id");
                    xmlWriter.WriteStartElement("violation");
                    xmlWriter.WriteStartElement("name");
                    xmlWriter.WriteString(violation.Name.ToString());
                    xmlWriter.WriteEndElement();
                    xmlWriter.WriteStartElement("avtoNumber");
                    xmlWriter.WriteString(violation.AvtoNumber.ToString());
                    xmlWriter.WriteEndElement();
                    xmlWriter.WriteStartElement("violationDate");
                    xmlWriter.WriteString(violation.ViolationDate.ToString("yyyy-MM-dd"));
                    xmlWriter.WriteEndElement();
                    xmlWriter.WriteStartElement("idNumber");
                    xmlWriter.WriteString(violation.IDNumber.ToString());
                    xmlWriter.WriteEndElement();
                    xmlWriter.WriteStartElement("fine");
                    xmlWriter.WriteString(violation.Fine.ToString());
                    xmlWriter.WriteEndElement();
                    xmlWriter.WriteStartElement("violationType");
                    xmlWriter.WriteString(violation.ViolationType.ToString());
                    xmlWriter.WriteEndElement();
                }
                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndDocument();
            }
        }

        private void CheckViolations(List<Violations> violations)
        {
            foreach (Violations violation in violations)
            {
                if (violation.Fine <= 0)
                {
                    throw new ArgumentException("Штраф не может быть меньше нуля");
                }
            }
        }

        private void CheckXML()
        {
            XmlSchemaSet schema = new XmlSchemaSet();
            schema.Add("", @"C:\Users\HP\source\repos\ConsoleApp1\Второй семестр\Labs\Violations.xsd");
            XmlReader rd = XmlReader.Create(Path);
            XDocument doc = XDocument.Load(rd);
            doc.Validate(schema, ValidationEventHandler);
        }

        static void ValidationEventHandler(object sender, ValidationEventArgs e)
        {
            XmlSeverityType type = XmlSeverityType.Warning;
            if (Enum.TryParse<XmlSeverityType>("Error", out type))
            {
                if (type == XmlSeverityType.Error) throw new Exception(e.Message);
            }
        }
    }
}

