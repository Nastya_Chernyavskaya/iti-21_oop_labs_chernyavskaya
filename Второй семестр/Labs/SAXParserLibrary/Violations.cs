﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAXParserLibrary
{
    public class Violations
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string AvtoNumber { get; set; }
        public DateTime ViolationDate { get; set; }
        public string IDNumber { get; set; }
        public decimal Fine { get; set; }
        public ViolationTypes ViolationType { get; set; }

        public Violations(int id, string name, string avtoNumber, DateTime violationDate, string idNumber, decimal fine, ViolationTypes violationType)
        {
            Id = id;
            Name = name;
            AvtoNumber = avtoNumber;
            ViolationDate = violationDate;
            IDNumber = idNumber;
            Fine = fine;
            ViolationType = violationType;
        }

        public enum ViolationTypes
        {
            Over_Speed_20_30,
            Over_Speed_30_40,
            Over_Speed_40,
            Drunk_Driving,
            Wrong_Stoping,
            Wrong_ParKing
        }

        //public override int GetHashCode()
        //{
        //    return Id ^ DepartureTime.Day ^ DepartureTime.Month ^ Destination.Length ^ (int)PlaneType;
        //}

        public override string ToString()
        {
            return "Id: " + Id + " | Name: " + Name + " | AvtoNumber: " + AvtoNumber + " | ViolationDate: " + ViolationDate + " | IDNumber: " + IDNumber + " | Fine: " + Fine + " | ViolationType: " + ViolationType;
        }
    }
}
