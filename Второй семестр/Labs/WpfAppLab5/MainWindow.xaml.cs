﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SAXParserLibrary;

namespace WpfAppLab5
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private SAXParser violationsParser;
        private List<Violations> violations;
        public MainWindow()
        {
            InitializeComponent();
            violations = new List<Violations>();
            violationTypeCBox.ItemsSource = Enum.GetValues(typeof(Violations.ViolationTypes)).Cast<Violations.ViolationTypes>().ToList();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog();
            dialog.Filter = "Text documents (*.xml)|*.xml";
            dialog.FilterIndex = 1;

            bool? result = dialog.ShowDialog();

            if (result == true)
            {
                string path = dialog.FileName;
                try
                {
                    violationsParser = new SAXParser(path);
                    violations = violationsParser.ParseXML();
                    filePathField.Content = "Path: " + violationsParser.Path;
                    violationsBox.ItemsSource = violations.Select(x => x.Id).ToList();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error");
                }
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                if (violationTypeCBox.SelectedItem != null)
                {
                    violations.FirstOrDefault(x => x.Id == Convert.ToInt32(violationsBox.SelectedItem)).Name = nameField.Text;
                    violations.FirstOrDefault(x => x.Id == Convert.ToInt32(violationsBox.SelectedItem)).AvtoNumber = avtoNumberField.Text;
                    violations.FirstOrDefault(x => x.Id == Convert.ToInt32(violationsBox.SelectedItem)).ViolationDate = DateTime.Parse(violationDateField.Text);
                    violations.FirstOrDefault(x => x.Id == Convert.ToInt32(violationsBox.SelectedItem)).IDNumber = idNumberField.Text;
                    violations.FirstOrDefault(x => x.Id == Convert.ToInt32(violationsBox.SelectedItem)).Fine = Decimal.Parse(fineField.Text);
                    violations.FirstOrDefault(x => x.Id == Convert.ToInt32(violationsBox.SelectedItem)).ViolationType = (Violations.ViolationTypes)violationTypeCBox.SelectedIndex;
                    violations.FirstOrDefault(x => x.Id == Convert.ToInt32(violationsBox.SelectedItem)).Id = Convert.ToInt32(idField.Text);
                    violationsParser.SaveXML(violations);
                    ClearFields();
                    violations = violationsParser.ParseXML();
                    violationsBox.ItemsSource = violations.Select(x => x.Id).ToList();
                }
                else if (violations != null && violationsParser != null)
                {
                    violationsParser.SaveXML(violations);
                    ClearFields();
                    violations = violationsParser.ParseXML();
                    violationsBox.ItemsSource = violations.Select(x => x.Id).ToList();
                }
            }
            catch (Exception ex)
            {
                violations = violationsParser.ParseXML();
                MessageBox.Show(ex.Message, "Error");
                ClearFields();
                violations = violationsParser.ParseXML();
                violationsBox.ItemsSource = violations.Select(x => x.Id).ToList();
            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            if (fileNameField.Text != "")
            {
                violationsParser = new SAXParser(@"D:\" + fileNameField.Text + ".xml");
                filePathField.Content = @"Path: D:\" + fileNameField.Text + ".xml";
                fileNameField.Text = "";
                violations = new List<Violations>();
                ClearFields();
            }
            else
            {
                MessageBox.Show("Enter file's name", "Error");
            }
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            if (idField.Text == "" || nameField.Text == "" || avtoNumberField.Text == "" || violationDateField.Text == "" || idNumberField.Text == "" || fineField.Text == "" || violationTypeCBox.SelectedItem == null)
            {
                MessageBox.Show("Поле пусто", "Ошибка");
            }
            else if (violations.Select(x => x.Id).ToList().Contains(Convert.ToInt32(idField.Text)))
            {
                MessageBox.Show("Id должен быть уникален");
            }
            else
            {
                try
                {
                    violations.Add(new Violations(Convert.ToInt32(idField.Text), nameField.Text, avtoNumberField.Text, DateTime.Parse(violationDateField.Text), idNumberField.Text, Decimal.Parse(fineField.Text), (Violations.ViolationTypes)violationTypeCBox.SelectedIndex));
                    ClearFields();
                    violationsBox.ItemsSource = violations.Select(x => x.Id).ToList();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error");
                }
            }
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            if (violationsBox.SelectedItem == null)
            {
                MessageBox.Show("Select flight to delete it", "Error");
            }
            else
            {
                violations.Remove(violations.FirstOrDefault(x => x.Id == Convert.ToInt32(violationsBox.SelectedItem)));
                ClearFields();
                violationsBox.ItemsSource = violations.Select(x => x.Id).ToList();
            }
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            ClearFields();
            filePathField.Content = "Path: Not selected";
            violations = null;
            violationsParser = null;
            violationsBox.ItemsSource = null;
        }

        private void ViolationsBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Convert.ToInt32(violationsBox.SelectedItem) > 0)
            {
                int id = Convert.ToInt32(violationsBox.SelectedItem.ToString());
                idField.Text = violations.FirstOrDefault(x => x.Id == id).Id.ToString();
                nameField.Text = violations.FirstOrDefault(x => x.Id == id).Name.ToString();
                avtoNumberField.Text = violations.FirstOrDefault(x => x.Id == id).AvtoNumber.ToString();
                violationDateField.Text = violations.FirstOrDefault(x => x.Id == id).ViolationDate.ToString("yyyy-MM-dd");
                idNumberField.Text = violations.FirstOrDefault(x => x.Id == id).IDNumber.ToString();
                fineField.Text = violations.FirstOrDefault(x => x.Id == id).Fine.ToString();
                violationTypeCBox.SelectedItem = violations.FirstOrDefault(x => x.Id == id).ViolationType;
            }
        }

        private void ClearFields()
        {
            idField.Text = default;
            nameField.Text = default;
            avtoNumberField.Text = default;
            violationDateField.Text = default;
            idNumberField.Text = default;
            fineField.Text = default;
            violationTypeCBox.ItemsSource = null;
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
