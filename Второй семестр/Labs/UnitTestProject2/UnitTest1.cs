using Microsoft.VisualStudio.TestTools.UnitTesting;
using QualityMetric;
using System;

namespace UnitTestQualityMetric
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            int result = 0;
            bool[] expect = new bool[] { true, true, true, true, true, true, true, true, true, true };
            bool[] real = new bool[] { true, true, true, true, true, true, true, true, true, true };
            for (int i = 0; i < expect.Length; i++)
            {
                if (expect[i] == true && real[i] == true)
                {
                    result++;
                }
            }
            Metric metric = new Metric(expect, real);
            Assert.AreEqual(result, metric.CounterTruePositive());
        }

        [TestMethod]
        public void TestMethod2()
        {
            int result = 0;
            bool[] expect = new bool[] { false, false, false, false, false, false, false, false, false, false };
            bool[] real = new bool[] { true, true, true, true, true, true, true, true, true, true };
            for (int i = 0; i < expect.Length; i++)
            {
                if (expect[i] == true && real[i] == true)
                {
                    result++;
                }
            }
            Metric metric = new Metric(expect, real);
            Assert.AreEqual(result, metric.CounterTruePositive());
        }

        [TestMethod]
        public void TestMethod3()
        {
            int result = 0;
            bool[] expect = new bool[] { false, false, false, false, false, false, false, false, false, false };
            bool[] real = new bool[] { false, false, false, false, false, false, false, false, false, false };
            for (int i = 0; i < expect.Length; i++)
            {
                if (expect[i] == true && real[i] == true)
                {
                    result++;
                }
            }
            Metric metric = new Metric(expect, real);
            Assert.AreEqual(result, metric.CounterTruePositive());
        }

        [TestMethod]
        public void TestMethod4()
        {         
            bool[] expect = new bool[] { false, false };
            bool[] real = new bool[] { false };
            Metric metric = new Metric(expect, real);
            Assert.ThrowsException<Exception>(() => metric.CounterTruePositive());
        }

        [TestMethod]
        public void TestMethod5()
        {
            int result = 0;
            bool[] expect = new bool[] { true, true, true, true, true, true, true, true, true, true };
            bool[] real = new bool[] { true, true, true, true, true, true, true, true, true, true };
            for (int i = 0; i < expect.Length; i++)
            {
                if (expect[i] == false && real[i] == true)
                {
                    result++;
                }
            }
            Metric metric = new Metric(expect, real);
            Assert.AreEqual(result, metric.CounterFalsePositive());
        }

        [TestMethod]
        public void TestMethod6()
        {
            int result = 0;
            bool[] expect = new bool[] { false, false, false, false, false, false, false, false, false, false };
            bool[] real = new bool[] { true, true, true, true, true, true, true, true, true, true };
            for (int i = 0; i < expect.Length; i++)
            {
                if (expect[i] == false && real[i] == true)
                {
                    result++;
                }
            }
            Metric metric = new Metric(expect, real);
            Assert.AreEqual(result, metric.CounterFalsePositive());
        }

        [TestMethod]
        public void TestMethod7()
        {
            int result = 0;
            bool[] expect = new bool[] { false, false, false, false, false, false, false, false, false, false };
            bool[] real = new bool[] { false, false, false, false, false, false, false, false, false, false };
            for (int i = 0; i < expect.Length; i++)
            {
                if (expect[i] == false && real[i] == true)
                {
                    result++;
                }
            }
            Metric metric = new Metric(expect, real);
            Assert.AreEqual(result, metric.CounterFalsePositive());
        }

        [TestMethod]
        public void TestMethod8()
        {
            bool[] expect = new bool[] { false, false };
            bool[] real = new bool[] { false };
            Metric metric = new Metric(expect, real);
            Assert.ThrowsException<Exception>(() => metric.CounterFalsePositive());
        }

        [TestMethod]
        public void TestMethod9()
        {
            int result = 0;
            Random random = new Random();
            bool[] expect = new bool[20];
            bool[] real = new bool[20];
            for (int i = 0; i < expect.Length; i++)
            {
                expect[i] = random.Next(0, 1) == 0 ? false: true;
                real[i] = random.Next(0, 1) == 0 ? false : true;
                if (expect[i] == true && real[i] == true)
                {
                    result++;
                }
            }
            Metric metric = new Metric(expect, real);
            Assert.AreEqual(result, metric.CounterTruePositive());
        }


        [TestMethod]
        public void TestMethod10()
        {
            int result = 0;
            Random random = new Random();
            bool[] expect = new bool[20];
            bool[] real = new bool[20];
            for (int i = 0; i < expect.Length; i++)
            {
                expect[i] = random.Next(0, 1) == 0 ? false : true;
                real[i] = random.Next(0, 1) == 0 ? false : true;
                if (expect[i] == false && real[i] == true)
                {
                    result++;
                }
            }
            Metric metric = new Metric(expect, real);
            Assert.AreEqual(result, metric.CounterFalsePositive());
        }
    }
}
