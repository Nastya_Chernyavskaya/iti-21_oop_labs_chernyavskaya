﻿using System;
using System.Data.Linq.Mapping;

namespace ViolationsCRUD
{
    [Table(Name = "Violations")]
    public class Violation
    {
        private ViolationType type;

        public Violation() => Id = -1;

        [Column(IsPrimaryKey = true, IsDbGenerated = true)]
        public int Id { get; internal set; }

        [Column]
        public int AutoNumber { get; set; }

        [Column]
        public DateTime Date { get; set; }
        
        public ViolationType Type { get => type; set => type = value; }

        [Column(Name = "ViolationTypeId")]
        private int TypeId
        {
            get => Type.Id;
            set
            {
                Type = ViolationTypeCRUD.Get(value);
            }
        }

        [Column]
        public decimal FineAmount { get; set; }

        [Column]
        public int DriverId { get; set; }

        public Inspector Inspector { get; set; }

        [Column(Name = "InspectorId")]
        private int InspectorId
        {
            get => Inspector.Id;
            set
            {
                Inspector = InspectorCRUD.Get(value);
            }
        }

        public override string ToString()
        {
            return string.Format($"Auto number: {AutoNumber} Date: {Date} Violation type: {Type} Fine amount: {FineAmount} " +
                $"Driver id: {DriverId} Inspector: {Inspector.FullName}");
        }
    }
}
