﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Linq;
using System.Linq;

namespace ViolationsCRUD
{
    public static class ViolationCRUD
    {
        public static void Create(Violation item)
        {
            using (DataContext db = new DataContext(ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString))
            {
                db.GetTable<Violation>().InsertOnSubmit(item);
                db.SubmitChanges();
            }
        }

        public static Violation Get(int id)
        {
            using (DataContext db = new DataContext(ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString))
            {
                return db.GetTable<Violation>().FirstOrDefault(x => x.Id == id);
            }
        }

        public static List<Violation> GetAll()
        {
            using (DataContext db = new DataContext(ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString))
            {
                return db.GetTable<Violation>().ToList();
            }
        }

        public static void Update(Violation item)
        {
            using (DataContext db = new DataContext(ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString))
            {
                var tmp = db.GetTable<Violation>().FirstOrDefault(x => item.Id == x.Id);
                if (tmp != null)
                {
                    tmp.AutoNumber = item.AutoNumber;
                    tmp.Date = item.Date;
                    tmp.DriverId = item.DriverId;
                    tmp.FineAmount = item.FineAmount;
                    tmp.Type = item.Type;
                    tmp.Inspector = item.Inspector;
                    db.SubmitChanges();
                }
            }
        }

        public static void Delete(Violation item)
        {
            using (DataContext db = new DataContext(ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString))
            {
                var tmp = db.GetTable<Violation>().FirstOrDefault(x => x.Id == item.Id);
                if (tmp != null)
                {
                    db.GetTable<Violation>().DeleteOnSubmit(tmp);
                    db.SubmitChanges();
                }
            }
        }
    }
}