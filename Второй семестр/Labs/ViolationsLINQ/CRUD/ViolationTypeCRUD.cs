﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Linq;
using System.Linq;

namespace ViolationsCRUD
{
    public static class ViolationTypeCRUD
    {
        public static void Create(ViolationType item)
        {
            using (DataContext db = new DataContext(ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString))
            {
                db.GetTable<ViolationType>().InsertOnSubmit(item);
                db.SubmitChanges();
            }
        }

        public static ViolationType Get(int id)
        {
            using (DataContext db = new DataContext(ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString))
            {
                return db.GetTable<ViolationType>().FirstOrDefault(x => x.Id == id);
            }
        }

        public static ViolationType Get(string name)
        {
            using (DataContext db = new DataContext(ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString))
            {
                return db.GetTable<ViolationType>().FirstOrDefault(x => x.Name == name);
            }
        }

        public static List<ViolationType> GetAll()
        {
            using (DataContext db = new DataContext(ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString))
            {
                return db.GetTable<ViolationType>().ToList();
            }
        }

        public static void Update(ViolationType item)
        {
            if (item.Id < 0)
            {
                throw new ApplicationException("Invalid item");
            }

            using (DataContext db = new DataContext(ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString))
            {
                var tmp = db.GetTable<ViolationType>().FirstOrDefault(x => item.Id == x.Id);
                if (tmp != null)
                {
                    tmp.Name = item.Name;
                    db.SubmitChanges();
                }
            }
        }

        public static void Delete(ViolationType item)
        {
            using (DataContext db = new DataContext(ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString))
            {
                var tmp = db.GetTable<ViolationType>().FirstOrDefault(x => x.Id == item.Id);
                if (tmp != null)
                {
                    db.GetTable<ViolationType>().DeleteOnSubmit(tmp);
                    db.SubmitChanges();
                }
            }
        }
    }
}
