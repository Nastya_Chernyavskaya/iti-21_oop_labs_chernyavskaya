﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Linq;
using System.Linq;

namespace ViolationsCRUD
{
    public static class InspectorCRUD
    {
        public static void Create(Inspector item)
        {
            using (DataContext db = new DataContext(ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString))
            {
                db.GetTable<Inspector>().InsertOnSubmit(item);
                db.SubmitChanges();
            }
        }

        public static Inspector Get(int id)
        {
            using (DataContext db = new DataContext(ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString))
            {
                return db.GetTable<Inspector>().FirstOrDefault(x => x.Id == id);
            }
        }

        public static Inspector Get(string name)
        {
            using (DataContext db = new DataContext(ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString))
            {
                return db.GetTable<Inspector>().FirstOrDefault(x => x.FullName == name);
            }
        }

        public static List<Inspector> GetAll()
        {
            using (DataContext db = new DataContext(ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString))
            {
                return db.GetTable<Inspector>().ToList();
            }
        }

        public static void Update(Inspector item)
        {
            using (DataContext db = new DataContext(ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString))
            {
                var tmp = db.GetTable<Inspector>().FirstOrDefault(x => item.Id == x.Id);
                if (tmp != null)
                {
                    tmp.FullName = item.FullName;
                    db.SubmitChanges();
                }
            }
        }

        public static void Delete(Inspector item)
        {
            using (DataContext db = new DataContext(ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString))
            {
                var tmp = db.GetTable<Inspector>().FirstOrDefault(x => x.Id == item.Id);
                if (tmp != null)
                {
                    db.GetTable<Inspector>().DeleteOnSubmit(tmp);
                    db.SubmitChanges();
                }
            }
        }
    }
}
