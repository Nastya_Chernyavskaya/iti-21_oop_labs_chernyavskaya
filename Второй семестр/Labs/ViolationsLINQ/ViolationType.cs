﻿using System.Data.Linq.Mapping;

namespace ViolationsCRUD
{
    [Table(Name = "ViolationTypes")]
    public class ViolationType
    {
        public ViolationType() => Id = -1;

        [Column(IsPrimaryKey = true, IsDbGenerated = true)]
        public int Id { get; internal set; }

        [Column(Name = "Type")]
        public string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
