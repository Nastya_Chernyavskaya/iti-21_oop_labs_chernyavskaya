﻿using System.Data.Linq.Mapping;

namespace ViolationsCRUD
{
    [Table(Name = "Inspectors")]
    public class Inspector
    {
        public Inspector() => Id = -1;

        [Column(IsPrimaryKey = true, IsDbGenerated = true)]
        public int Id { get; internal set; }

        [Column]
        public string FullName { get; set; }
    }
}