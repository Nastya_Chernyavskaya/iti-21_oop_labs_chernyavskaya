﻿namespace ViolationsCRUD
{
    public class ViolationType
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
