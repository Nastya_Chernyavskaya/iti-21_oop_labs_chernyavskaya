﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ViolationsCRUD;

namespace WFA7
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            listBox1.Items.AddRange(ViolationCRUD.GetAll().ToArray());
            listBox2.Items.AddRange(ViolationTypeCRUD.GetAll().ToArray());
            comboBox1.DataSource = listBox2.Items;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (textBox5.Text.Length > 0)
            {
                var tmp = new ViolationType() { Name = textBox5.Text };
                ViolationTypeCRUD.Create(tmp);
                listBox2.Items.Add(tmp);
                comboBox1.DataSource = null;
                comboBox1.DataSource = listBox2.Items;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            var item = listBox2.SelectedItem as ViolationType;
            if (item != null)
            {
                item.Name = textBox5.Text;
                ViolationTypeCRUD.Update(item);
                listBox2.Items[listBox2.SelectedIndex] = item;
                comboBox1.DataSource = null;
                comboBox1.DataSource = listBox2.Items;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var item = listBox2.SelectedItem as ViolationType;
            if (item != null)
            {
                ViolationTypeCRUD.Delete(item);
                listBox2.Items.Remove(item);
                comboBox1.DataSource = null;
                comboBox1.DataSource = listBox2.Items;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var viol = new Violation()
                {
                    AutoNumber = int.Parse(textBox1.Text),
                    Date = dateTimePicker1.Value,
                    DriverId = int.Parse(textBox3.Text),
                    FineAmount = decimal.Parse(textBox2.Text),
                    Type = (ViolationType)comboBox1.SelectedItem
                };
                var tmp = InspectorCRUD.Get(textBox4.Text);
                if (tmp == null)
                {
                    tmp = new Inspector()
                    {
                        FullName = textBox4.Text
                    };
                    InspectorCRUD.Create(tmp);

                }
                viol.Inspector = tmp;
                ViolationCRUD.Create(viol);
                listBox1.Items.Add(viol);
            }
            catch
            {
                MessageBox.Show("Incorrect input");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                var viol = listBox1.SelectedItem as Violation;
                if (viol != null)
                {
                    viol.AutoNumber = int.Parse(textBox1.Text);
                    viol.Date = dateTimePicker1.Value;
                    viol.DriverId = int.Parse(textBox3.Text);
                    viol.FineAmount = decimal.Parse(textBox2.Text);
                    viol.Type = (ViolationType)comboBox1.SelectedItem;
                    var tmp = InspectorCRUD.Get(textBox4.Text);
                    if (tmp == null)
                    {
                        tmp = new Inspector()
                        {
                            FullName = textBox4.Text
                        };
                        InspectorCRUD.Create(tmp);
                    }
                    viol.Inspector = tmp;
                    ViolationCRUD.Update(viol);
                    listBox1.Items[listBox1.SelectedIndex] = viol;
                }
            }
            catch
            {
                MessageBox.Show("Incorrect input");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var viol = listBox1.SelectedItem as Violation;
            if (viol != null)
            {
                ViolationCRUD.Delete(viol);
                listBox1.Items.Remove(viol);
            }
        }
    }
}
